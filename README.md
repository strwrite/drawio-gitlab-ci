# Draw.io -> Gitlab-CI Integration How To

Draw diagrams with [draw.io](https://draw.io)          |  Export to images with Gitlab CI |   Use result in docs
:-------------------------:|:-------------------------:|:-------------------------:
![Here should be screenshot of draw.io](./example/screenshots/draw.png) |  ![Here should be gitlab job run](example/screenshots/gitlab.png) | <img src="example/out/how-to-use-draw-io-with-gitlab-ci.png" width="1200"  alt="Here should be exported image" /> 

 <img src="example/out/how-to-use-draw-io-with-gitlab-ci.png" alt="Here should be exported image" /> 

# Requirements

These requirements are only for running local examples and dubugging your own setup:

 - docker
 - gitlab-runner
 - make

To actually use drawio-gitlab-ci exporting process you don't need to install anything additionally except Git and browser ;)

# Usage

## Example

Clone sources to get example:

```
git clone https://gitlab.com/strwrite/drawio-gitlab-ci
cd drawio-gitlab-ci
```

Remove folder out:

``` bash
rm example/out -r
```

``` bash
make run-example
```

### What just happened?

Make command was:

``` bash
gitlab-runner exec docker export --docker-volumes ${current_dir}/example/out:/out
```

where current_dir defined as
``` Makefile
current_dir = $(shell pwd)
```

Gitlab Runner's `export` job defined as 

``` yml
export:
  image: strowk/drawio-export-docker
  script:
    - drawio example/ping-pong.xml -o /out/ping-pong.png
    - drawio example/how-to-buy-a-car.xml -o /out/how-to-buy-a-car.png
```

Image `strowk/drawio-export-docker` is built from content of `drawio-export-docker` folder.

Basically it just contains [draw.io-export](https://www.npmjs.com/package/draw.io-export) 
npm module and it's dependencies.

Files `example/ping-pong.xml` and `example/how-to-buy-a-car.xml` contain diagrams made with 
[draw.io](https://draw.io). You can try modify them there, commit changes (!) to xml files
and rerun `make run-example`. 

Files in out directory should be changed. Change in your local directory happens thanks to 
`--docker-volumes` which we passed for `gitlab-runner` execution.  

## How to use in my pipeline?

You can just use example snippet from above:

``` yml
export:
  image: strowk/drawio-export-docker
  script:
    - drawio example/ping-pong.xml -o /out/ping-pong.png
    - drawio example/how-to-buy-a-car.xml -o /out/how-to-buy-a-car.png
```

in your `.gitlab-ci.yml` , but providing your own files and maybe publish 
resulting files in artifacts, Gitlab Pages or other integrated systems. 

## How to commit exported images to Git?

Simplest way to distribute resulting pictures is to commit them to the 
same repository, where you keep draw.io sources and run Gitlab CI.

For that .gitlab-ci.yml has the second job `export-and-commit`. 

Here's it:

``` yml
export-and-commit:
  image: strowk/drawio-export-docker
  script:
    - if [[ -f /.gitciexec.env ]] ; then source /.gitciexec.env ; else echo 'no local env, skip apply' ; fi
    - git remote set-url origin https://str.write:${CI_PUSH_TOKEN}@gitlab.com/strwrite/drawio-gitlab-ci.git
    - git config --global user.email 'weekendbegin@gmail.com'
    - git config --global user.name 'str.write'
    - git checkout -B master
    - drawio example/ping-pong.xml -o example/out/ping-pong.png
    - drawio example/how-to-buy-a-car.xml -o example/out/how-to-buy-a-car.png
    - drawio example/how-to-use-draw-io-with-gitlab-ci.xml -o example/out/how-to-use-draw-io-with-gitlab-ci.png
    - git add example/out
    - git commit -m '[skip ci] draw.io images auto-updating' || echo 'nothing to commit, skip'
    - git push --follow-tags origin master
  only: ['master']
```

This job is actually running on Gitlab CI pipeline of this project and updates pictures, 
which you've seen earlier.

This is a bit more trickier than what we've done before.
First thing is 

``` bash
if [[ -f /.gitciexec.env ]] ; then source /.gitciexec.env ; else echo 'no local env, skip apply' ; fi
```

This oneliner helps me with local executions using `gitlab-runner exec` command. 
I added this `.gitciexec.env` in .gitignore file and filled the file like that:

``` bash
export CI_PUSH_TOKEN=<TOKEN>
```

, changing `<TOKEN>` to the API token received from Gitlab. 
Same variable `CI_PUSH_TOKEN` was configured in protected variable in Gitlab. 

Following 

``` bash
git remote set-url origin https://str.write:${CI_PUSH_TOKEN}@gitlab.com/strwrite/drawio-gitlab-ci.git
git config --global user.email 'weekendbegin@gmail.com'
git config --global user.name 'str.write'
git checkout -B master
```

is just git configurations, needed to commit to Git repo.  
If using this snippet, don't forget to pass your username, repo and desired branch.

``` bash 
drawio example/ping-pong.xml -o example/out/ping-pong.png
drawio example/how-to-buy-a-car.xml -o example/out/how-to-buy-a-car.png
```

As previously now we run export to pictures, only this time we peserve the same directories 
structure (use `example/out/` instead `/out`). 

Finally once changes are appeared (or nothing was changed), we need to add, commit and push 
them to Git. 

``` bash 
git add example/out
git commit -m '[skip ci] draw.io images auto-updating' || echo 'nothing to commit, skip'
git push --follow-tags origin master
```

Important part here are `[skip ci]` - to make Gitlab skip this commit when deciding
to run CI, otherwise it would continue running. 

`|| echo 'nothing to commit, skip'` helps us to not fail in case if nothing was changed. 

# Roadmap

Marked `(?)` where not sure if possible to do it.

 - add wrapper for additional configurations: 
   - support folder as an input
   - support more output configurations
 - add howtos to publish to gitlab pages (use some SSG)
 - (?) use `mxgraph` directly without browser and pupeteer
   - reduce image size (node on alpine)
 - (?) support url export to produce Markdown snippets