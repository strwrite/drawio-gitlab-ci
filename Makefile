# update-submodules:
	# git submodule update --init --recursive --remote

build:
	(cd drawio-export-docker ; docker build -t strowk/drawio-export-docker .)

build-push: build
	docker push strowk/drawio-export-docker

current_dir = $(shell pwd)
run-docker-example:
	docker run --rm -v ${current_dir}:/src strowk/drawio-export-docker \
	drawio example/ping-pong.xml -o example/ping-pong.png 
shell-docker-example:
	docker run --name drawio-export --rm -v ${current_dir}:/src -ti strowk/drawio-export-docker bash
cp-export-js:
	docker cp drawio-export:/usr/local/lib/node_modules/draw.io-export/export.js ./drawio-export-docker/export.js


build-run: build run-docker-example

run-example:
	(gitlab-runner exec docker export --docker-volumes ${current_dir}/example/out:/out)

run-commit-example:
	(gitlab-runner exec docker export-and-commit --docker-volumes ${current_dir}/.gitciexec.env:/.gitciexec.env)

TO_EXPORT ?= example

export:
	(gitlab-runner exec docker export)